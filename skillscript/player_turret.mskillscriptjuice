include ./propertysheet/player_core.mskillscriptpropertysheet
include "rogue/game system data/rejuice/player_turret_mods.rejuice"
include "rogue/game system data/rejuice/skillrankvariables.rejuice"

SkillScript player_turret < uid=F232F0B55220ADA4000009E5EA0F7C4C >
{
	myGraphFilename ./player_turret.mskillscriptgraph
	myPropertySheet < uid=699180B9530C9FF200000A9AFCC181CF > : player_core
	{
		myCanRunOnDownedAgent TRUE
		myCanRunOnSprintingAgent TRUE
		myCanRunOnAgentInNavigationalInteraction TRUE
		myCanRunWhilstAiming FALSE
		myCanRunWhilstRolling TRUE
		myCanRunWhilstUsingShield TRUE
		myCanRunWhilstAimingGrenades TRUE
	}
	myUIName "contextComment = \"UI name for the turret skill.\", description = \"TurretSkill_Name\", enabled = true, guid = #F232F0B55228A68B00000DAC73595D0B, lineVersion = 0, maxLength = 1, text = \"Automated Turret\""
	myToolTipText "contextComment = \"Tooltip for the turret skill.\", description = \"TurretSkill_Tooltip\", enabled = true, guid = #F232F0B55228A68B00000DADE4C2FD8E, lineVersion = 1, maxLength = 1, text = \"A stationary turret that shoots at nearby hostiles until it is destroyed or until the battery runs out.\""
	myControlCategoryList
	{
		ControlCategory "(Before Deployment)"
		{
			myTitle "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FCFFB00004F07E417B7CA, lineVersion = 1, maxLength = 22, text = \"Before Deployment\""
			myControlList
			{
				ControlProperty "Tap Place"
				{
					myButtonPrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD02B00004F0B2BB85D4E, lineVersion = 1, maxLength = 11, text = \"Tap {0}\""
					myKeyboardMousePrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #B6EDF43A56168A8C00007C4615F9DBF5, lineVersion = 1, maxLength = 18, text = \"Double Tap {0}\""
					myAction "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD02B00004F0D561FEE28, lineVersion = 0, maxLength = 1, text = \"Place\""
				}
				ControlProperty "Hold Aim"
				{
					myButtonPrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD04E00004F135B7ADC83, lineVersion = 1, maxLength = 13, text = \"Hold {0}\""
					myKeyboardMousePrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #B6EDF43A56168AAD00007C59F3C5237F, lineVersion = 0, maxLength = 1, text = \"{0}\""
					myAction "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD04E00004F15B08FB68F, lineVersion = 0, maxLength = 1, text = \"Aim\""
				}
				ControlProperty "Cancel Aim"
				{
					myButtonPrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD07700004F1BB5DF1CB1, lineVersion = 1, maxLength = 52, text = \"Hold {0} + Tap <gameaction src=UICancel>\""
					myKeyboardMousePrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #B6EDF43A56168B6F00007CBFB5682995, lineVersion = 0, maxLength = 1, text = \"<gameaction src=SkillScriptCancelSkill>\""
					myAction "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD07700004F1D1EE13ACF, lineVersion = 0, maxLength = 1, text = \"Cancel\""
				}
				ControlProperty "Release Throw"
				{
					myButtonPrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD0F700004F230E120978, lineVersion = 1, maxLength = 14, text = \"Release {0}\""
					myKeyboardMousePrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #B6EDF43A56168BAF00007CE22F728AA6, lineVersion = 0, maxLength = 1, text = \"<gameaction src=SkillScriptShootRelease>\""
					myAction "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD0F700004F2513A6E016, lineVersion = 0, maxLength = 1, text = \"Throw\""
				}
			}
		}
		ControlCategory "(After Deployment)"
		{
			myTitle "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD11400004F2B02C3591B, lineVersion = 1, maxLength = 21, text = \"After Deployment\""
			myControlList
			{
				ControlProperty "Tap Disable"
				{
					myButtonPrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD12500004F2F5E9C81B3, lineVersion = 1, maxLength = 11, text = \"Tap {0}\""
					myKeyboardMousePrompt "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #B6EDF43A56168C6800007D3CFC4660F6, lineVersion = 0, maxLength = 1, text = \"{0}\""
					myAction "contextComment = \"Skill controls tooltip\", description = \"Skill controls tooltip\", enabled = true, guid = #C9B4D1A1550FD12500004F3114636F84, lineVersion = 0, maxLength = 1, text = \"Disable\""
				}
			}
		}
	}
	myIconSprite "mySpriteId = 947219868, mySpritesheet = \"rogue/ui/spritesheets/UI_Skill_Icon_Spritesheet_02.spritesheet\""
	myIconFile rogue/ui/Skills/skillIcon_automatedTurret.png
	myMods
	{
		SkillScriptMod "Precision Targeting" < uid=CD271A01547D989E00002EDB6BD8B250 > = "Precision Targeting"
		SkillScriptMod Flamethrower < uid=CD271A01547EF27500002F0DBE54728B > = Flamethrower
		SkillScriptMod Taser < uid=CD271A01548EB55000002EB59C0DD78D > = Taser
		SkillScriptMod "Self Destruct" < uid=CD271A01548EBC4900002EAB1A571113 > = "Self Destruct"
	}
	myPreloadModelList
	{
		PreloadedModel Backpack
		{
			myPreloadModelFilename "rogue/graph objects/gear/BP_turret.mgraphobject"
		}
		PreloadedModel "Basic Turret"
		{
			myPreloadModelFilename "rogue/graph objects/character/SK_Turret.mgraphobject"
		}
		PreloadedModel "Flamer Turret"
		{
			myPreloadModelFilename "rogue/graph objects/character/SK_Turret_Flamer.mgraphobject"
		}
	}
	myDroneProperties
	{
		myExecuteGraph TRUE
	}
	myEquipSoundSwitches
	{
		myOnEquip
		{
			myGuid 46F411B40E9150A5963818B0FD8A48B8
		}
		myOnUnequip
		{
			myGuid 45DF172B0F52EBC94358236F0EC8A089
		}
	}
	myEquipUISoundEvents
	{
		myOnEquipUI
		{
			myGuid 476E37D33A4A39C64B02C5D8E52DCB8A
		}
	}
	mySkillRankList
	{
		SkillRank "Rank 1 Base"
		{
			myLevel 1
			myRankVariableList
			{
				SkillRankVariable Damage
				{
					myUID < uid=CD271A015486CAB600002DF62A6C9021 > = Turret_Damage
				}
				SkillRankVariable "Firing Range"
				{
					myUID < uid=CD271A015486CAB600002DF7C060E7AA > = Turret_FireRange
				}
				SkillRankVariable Lifetime
				{
					myUID < uid=CD271A015486CAB600002DF9272A8C8B > = Turret_Lifetime
				}
				SkillRankVariable Health
				{
					myUID < uid=CD271A015486CAB600002DFA6989FDD7 > = Turret_Health
				}
			}
		}
		SkillRank "Rank 2"
		{
			myLevel 2
			myRankVariableList
			{
				SkillRankVariable "+Damage L1"
				{
					myUID < uid=CD271A015486CB2400002E03D738882D > = Turret_Damage
					myValue 0.125
				}
				SkillRankVariable "Firing Range"
				{
					myUID < uid=C9B4D1A15582A24C00005CF04F291811 > = Turret_FireRange
				}
				SkillRankVariable Lifetime
				{
					myUID < uid=C9B4D1A15582A24C00005CF125414FD8 > = Turret_Lifetime
				}
				SkillRankVariable Health
				{
					myUID < uid=C9B4D1A15582A24C00005CF2906D3AC8 > = Turret_Health
				}
			}
		}
		SkillRank "Rank 3"
		{
			myLevel 3
			myRankVariableList
			{
				SkillRankVariable "Damage L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF31153C160 > = Turret_Damage
					myValue 0.125
				}
				SkillRankVariable "+Firing Range L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF416BDBACB > = Turret_FireRange
					myValue 0.125
				}
				SkillRankVariable Lifetime
				{
					myUID < uid=CD271A015486CB2400002E0413B58EF8 > = Turret_Lifetime
					myValue 0.0
				}
				SkillRankVariable Health
				{
					myUID < uid=CD271A015486CB2400002E054C94CDCD > = Turret_Health
					myValue 0.0
				}
			}
		}
		SkillRank "Rank 4"
		{
			myLevel 4
			myRankVariableList
			{
				SkillRankVariable "Damage L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF539AD084B > = Turret_Damage
					myValue 0.125
				}
				SkillRankVariable "Firing Range L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF6C0EFCECC > = Turret_FireRange
					myValue 0.125
				}
				SkillRankVariable "+Lifetime L1"
				{
					myUID < uid=CD271A015486CB6300002E0C094F65F6 > = Turret_Lifetime
					myValue 0.25
				}
				SkillRankVariable "+Health L1"
				{
					myUID < uid=CD271A015486CB6300002E0DE888C612 > = Turret_Health
					myValue 0.25
				}
			}
		}
		SkillRank "Rank 5"
		{
			myLevel 5
			myRankVariableList
			{
				SkillRankVariable "+Damage L2"
				{
					myUID < uid=CD271A015486CB6300002E0E0561850C > = Turret_Damage
					myValue 0.25
				}
				SkillRankVariable "Firing Range L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF74F384F70 > = Turret_FireRange
					myValue 0.125
				}
				SkillRankVariable "Lifetime L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF8B4A8CFBE > = Turret_Lifetime
					myValue 0.25
				}
				SkillRankVariable "Health L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CF9E5166630 > = Turret_Health
					myValue 0.25
				}
			}
		}
		SkillRank "Rank 6"
		{
			myLevel 6
			myRankVariableList
			{
				SkillRankVariable "Damage L2"
				{
					myUID < uid=C9B4D1A15582A24C00005CFA5B314DF0 > = Turret_Damage
					myValue 0.25
				}
				SkillRankVariable "+Firing Range L2"
				{
					myUID < uid=CD271A015486CB9700002E116B2CC8D1 > = Turret_FireRange
					myValue 0.25
				}
				SkillRankVariable "Lifetime L1"
				{
					myUID < uid=CD271A015486CB9700002E12626C695D > = Turret_Lifetime
					myValue 0.25
				}
				SkillRankVariable "Health L1"
				{
					myUID < uid=C9B4D1A15582A24C00005CFBD6FFCA29 > = Turret_Health
					myValue 0.25
				}
			}
		}
		SkillRank "Rank 7"
		{
			myLevel 7
			myRankVariableList
			{
				SkillRankVariable "Damage L2"
				{
					myUID < uid=C9B4D1A15582A24C00005CFC24D83A77 > = Turret_Damage
					myValue 0.25
				}
				SkillRankVariable "Firing Range L2"
				{
					myUID < uid=C9B4D1A15582A24C00005CFD72A9FF3A > = Turret_FireRange
					myValue 0.25
				}
				SkillRankVariable "+Lifetime L2"
				{
					myUID < uid=CD271A015486CBE100002E17DC348439 > = Turret_Lifetime
					myValue 0.5
				}
				SkillRankVariable "+Health L2"
				{
					myUID < uid=CD271A015486CBE100002E18664AC349 > = Turret_Health
					myValue 0.5
				}
			}
		}
		SkillRank "Rank 8 Master"
		{
			myLevel 8
			myRankVariableList
			{
				SkillRankVariable "+Damage L3"
				{
					myUID < uid=CD271A015486CBE100002E19E8B61765 > = Turret_Damage
					myValue 0.4
				}
				SkillRankVariable "+Firing Range L3"
				{
					myUID < uid=C9B4D1A15582A24C00005CFEA3266252 > = Turret_FireRange
					myValue 0.4
				}
				SkillRankVariable "Lifetime L2"
				{
					myUID < uid=C9B4D1A15582A24C00005CFF8BA05A9D > = Turret_Lifetime
					myValue 0.5
				}
				SkillRankVariable "Health L2"
				{
					myUID < uid=C9B4D1A15582A24C00005D006E4EC21C > = Turret_Health
					myValue 0.5
				}
			}
		}
	}
	myArchetype Defensive
	mySkillScriptType Turret
	myUIAttributeList
	{
		uid Damage C9B4D1A15609161B00000771BD22DC83
		uid Range C9B4D1A156091CFE0000074F4A90CEBE
		uid Lifetime C9B4D1A156091DAD00000758DB934E70
		uid Health C9B4D1A156091DAD000007598E075647
		uid Cooldown C9B4D1A1562F6BAF0000AB6867318137
	}
	myUIAttributeGearList
	{
		uid Damage C9B4D1A156090D56000007543878F2B1
		uid Range C9B4D1A15609161B0000076C97E90D6B
		uid Lifetime C9B4D1A15609161B0000076ED086B0EC
		uid Health C9B4D1A15609161B000007708FD9B4C4
		uid Cooldown C9B4D1A1562F6BAF0000AB6765BA7AF8
	}
	myUIAttributeSkillPowerList
	{
		uid Damage C9B4D1A155ED9C2B000080933F609793
		uid Range C9B4D1A155ED9D22000080A0E87013D9
		uid Lifetime C9B4D1A155ED9E2A000080B3DB20CDCC
		uid Health C9B4D1A155ED9E55000080BB00253B72
		uid Cooldown C9B4D1A1562F6BAF0000AB66F9947E90
	}
	myUIAttributeFinalList
	{
		uid Damage C9B4D1A15657391C0000CCD489B187F1
		uid Range C9B4D1A155ED9D22000080A1D3BF983C
		uid Lifetime C9B4D1A155ED9E2A000080B4835F71A5
		uid Health C9B4D1A156581694000095DA14BFB58E
		uid Cooldown C9B4D1A1562F6BAF0000AB6953373BA7
	}
	myKeyboardToggles TRUE
}

