I stopped playing this game due to how a Massive mess (pun intended) and how I feel betrayed over 3 year of waiting for a lore-porn, immersive experience (I do take notice at the [x days ago](http://www.gameranx.com/img/14-Jun/the-division---angry-interview-e3-2014720ph264-aacmp4snapshot045320140621224738.jpg), MSV). So far none. 

Gameplay? I can't even enjoy either as a DPS gunner or a Skiller to unleash BFG sticky. Underpowered. Looting is like a narcissitic parent shit on everything you made in life only for a few shiny lights feels truly shiny. That's abusive to your players.

Since the game is a teamwork product, the whole team is at fault.

I might update this repo someday, but won't be regular as before.

###Latest note: Loot Weight###

[I explained a bit about loot weight right here and example on how it works with the Caduceus](https://www.reddit.com/r/thedivision/comments/4p482s/caduceus_drop_report_p12/d4i227u)


#ABOUT THIS REPO#

##Pretty much this is a stripped version of The Division datas (not source code), without baked assets and other multimedia assets.##

### This should include ###

* Item Stats/Blueprint
* Drop Table/Loot Chances
* AI / HVT
* UI formulas (DPS, Toughness, Skills, etc)
* Much more unreadable jargons

##Please don't fork this.##

###Console scripts and old files###

Regarding missing of console scripts folder, MSV has stopped putting scripts in there so I removed it. If you want to see some of them, you can check out 1.1 branch of repo.

###REDDIT/VG247/TCDG/UBI FORUM READERS###

If you want in-depth explaination from other dataminers, here's a few people you can find and talk to (they are not related to me, they are just people who have been datamined before and understanding what is happening inside the game other than Massive devs):

- Discord Channel: Total#3514 @ #theorycrafting, @Penoxi#4162 , and DocHolliday324

- Reddit: /u/penox, /u/cmm324, or if you want to talk to me /u/PixeIs (Formerly /u/t149 but I lost my password)

Additional: [I don't trust /u/spydr101 even when he's known to datamine before me, he does getting inaccurate assumptions a lot more](https://www.reddit.com/r/thedivision/comments/4nbllu/notes_from_datamine_20160609/d431t8g).

# NOTES for dummy #


###Caduceus###

The Caduceus drops on every named NPC (except for Falcon Lost APC - Warlord/Clear Sky Tank - Medved, Blisscopter, and Brooklyn).

It **doesn't matter** if you are in Free Roam or Mission. Nor the missions difficulty (Nope, it's unaffected).

Caduceus has it's own weapon type (being exotic it got default to HE drop), hence rendering Scavenging useless, so don't even bother to equip that.

###Scavenging###

"An item is dropped, then quality is rolled for. 
Scavenging will increase the chance that this roll ends up at a higher quality (by quality I mean colour: superior, high-end etc). "

The math will apply to all items that have an inherent quality which means crafting materials too. (Also, it does loops)

Scavenging doesn't increase loot quantity or better chance to drop Exotics. They are at RNGesus mercy.