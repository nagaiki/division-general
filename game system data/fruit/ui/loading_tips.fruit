include "rogue/game system data/fruit/ui/loading_tipconditions.fruit"
include "rogue/game system data/fruit/ui/loading_tiptriggers.fruit"
include "rogue/game system data/fruit/platforms.fruit"

enum GameModesAvailable
{
	DarkZone
	Campaign
	Both
}

// Please keep me in sync with the enum HelpMenuCategory in RClient_LoadingTipDefines.h
// Add new ones in the order you want them to show in the help menu and update ui-helpmenu.muigraph accordingly.
enum HelpMenuCategory
{
	BaseOfOperations
	Combat
	Consumables
	Contamination
	Coop
	Darkzone
	Economy
	Exploration
	Inventory
	Missions
	Navigation
	NPCs
	Options
	RPG
	Skills
	Social
	DLC_Underground
	Weapons
	None
}

class FullScreenTip
{
	string myImage
	varmap myDescription < bhv="localization" >
}

list FullScreenList
{
	FullScreenTip
}

class LoadingTip < uid=auto tooltip="An instance of something the player can be taught." >
{
	varmap myHintHeader < bhv="localization" tooltip="The title of the tip that will be displayed to the player." >
	varmap myHintDescription < bhv="localization" tooltip="The description of the tip that will be displayed to the player." >
	varmap myOptionalHintDescriptionKeyboard < bhv="localization" tooltip="[Optional]The description of the tip that will be displayed to the player, if they are using a keyboard and mouse." >
	string myIconSprite < bhv="sprite" tooltip="Icon that will be shown with the tip." >	
	float myPriority 1.0 < min=1.0 max=50.0 tooltip="Used to help apply weighting to the display order of tips. Higher numbers show greater priority." >
	float myTeachLimit 40.0 < min=0.0 tooltip="Value that causes a subject to be redisplayed if the modified learning points by the time is less than this value.0 means this limit will not be taken into account" >
	float myPassLimit 80.0 < min=0.0 tooltip="Value that causes a subject to not be displayed to the user when the total unmodified learning points is greater than this value. 0 means this limit will not be taken into account" >
	float myTeachCooldown 60.0 < min=1.0 tooltip="Minimum amount of time (in seconds) to wait until we can display this tip again." >
	float myUseRecordCooldown 5.0 < min=1.0 tooltip="Minimum amount of time (in seconds) to wait after displaying a tip, before using the subject in question can award learning points." >
	float myDisplayDuration 10.0 < min=3.0 max=60.0 tooltip="Length of time (in seconds) to display this tip for." >
	bool myIgnoresGlobalTeachCooldown FALSE < tooltip="If TRUE, tip will be processed as soon as possible, even if the global teach cooldown is active." >
	bool myDisplayUntilActionIsPerformed FALSE < tooltip="If TRUE, the tip will remain onscreen and no new tip will be displayed until the action for this tip is performed while it is onscreen." >
	GameModesAvailable myAvailableGameMode Both < tooltip="Choose the game mode this tip can be displayed in." >
	bool myIsDisplayedInDeadScreen FALSE < tooltip="Set to TRUE if this tip can be displayed in the death/respawn screen." >
	bool myIsDisplayedInLoadingScreen FALSE < tooltip="Set to TRUE if this tip can be displayed in the loading screens." >
	bool myIsDisplayedInFastTravel FALSE < tooltip="Set to TRUE if this tip can be displayed during fast travel." >
	bool myIsDisplayedInGame FALSE < tooltip="Set to TRUE if this tip can be displayed during general gameplay." >
	CiceroTrigger* myOnUseTrigger < tooltip="Used to link this subject to a particular game action only using the client information (to report subject usage)" >	
	CiceroConditionList myConditions < tooltip="A list of conditions to control when this tip is displayed." >
	float myWaitForUseTime 0.0f < min=0.0 max=120.0 tooltip="Time the tip will wait before displaying if conditions are met" >
	PlatformList myExcludedPlatforms < tooltip="Tip will not be loaded on platforms in this list" >
	HelpMenuCategory myHelpMenuCategory None < tooltip="Set which help menu category this tip belongs to. Set to None to not show in help menu." >
	FullScreenList myFullScreenList < tooltip="List of image and string pairs to be used for fullscreen tips." >
	bool myIsMatchmakingTutorial < tooltip="If set to true will enable quick matchmaking function in the UI." >
}

list LoadingTipList
{
	LoadingTip
}
