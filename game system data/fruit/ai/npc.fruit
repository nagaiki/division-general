include "rogue/game system data/fruit/attribute/attribute_progression.fruit"
include "rogue/game system data/fruit/npcgear/npcgear.fruit"
include "rogue/game system data/fruit/ai/npctypes.fruit"
include "rogue/game system data/fruit/loot/loottables.fruit"
include "rogue/game system data/fruit/ai/aimprofiles.fruit"
include "rogue/game system data/fruit/ai/ai.fruit"
include "rogue/game system data/fruit/ai/detectionprofiles.fruit"
include "rogue/game system data/fruit/ai/noncombatantdetectionprofiles.fruit"
include "rogue/game system data/fruit/ai/motion/motionpattern.fruit"
include "rogue/game system data/fruit/ai/npccombatconfig.fruit"
include "rogue/game system data/fruit/ai/threatprofiles.fruit"
include "rogue/game system data/fruit/ai/ailife/ailifeconfig.fruit"
include "rogue/game system data/fruit/ai/shootsequenceprofiles.fruit"
include "rogue/game system data/fruit/ai/motion/patternprofiles.fruit"
include "rogue/game system data/fruit/bodypart.fruit"
include "rogue/game system data/fruit/faction/faction.fruit"
include "rogue/game system data/fruit/ai/motion/motionenums.fruit"
include "rogue/game system data/fruit/ai/navigationcapabilities.fruit"
include "rogue/game system data/fruit/ai/snaptogroundtype.fruit"
include "rogue/game system data/fruit/animation/animsys.fruit"
include "rogue/game system data/fruit/physics/physicscharacterprofiles.fruit"
include "rogue/skillscript/skillscript.fruit"
include "modules/core/audio/audio.fruit"

list QuestList
{
	file 
}

list PrimaryWeapons
{
	file < bhv=asset assettype=item >
}

list SecondaryWeapons
{
	file < bhv=asset assettype=item >
}

list ArmorPieces
{
	file < bhv=asset assettype=item >
}

class QuestGiver
{
	QuestList myQuestList
	locstring myQuestWelcomeMessage
}

class WeakPoint
{
	BodyPart* myBodyPart
	string myStaggerGaugeName
	
	SkillScript* mySkillScriptTriggerAction
}

list WeakPointList
{
	WeakPoint
}

list DialogueCharacterList
{
	string < bhv="dialogueCharacter" >
}

class NPCSceneData
{
	SequenceNPCType mySequenceNPCType TwoHanded
}

class VisualGenerationWeights
{
	float myEthnicityCaucasianWeight 1 < min=0 max=10 >
	float myEthnicityAsianWeight 1 < min=0 max=10 >
	float myEthnicityAfricanAmericanWeight 1 < min=0 max=10 >
	float myEthnicityHispanicWeight 1 < min=0 max=10 >

	float myHeadCoverageNakedWeight 1 < min=0 max=10 >
	float myHeadCoverageHatWeight 1 < min=0 max=10 >
	float myHeadCoverageChestWeight 1 < min=0 max=10 >

	float myHandCoverageNakedWeight 1 < min=0 max=10 >
	float myHandCoverageChestWeight 1 < min=0 max=10 >
	
	float myPersonalityStandardWeight < min=0.0 default=1.0 >
	float myPersonalityBraveWeight < min=0.0 default=0.0 >
	float myPersonalityDefiantWeight < min=0.0 default=0.0 >
	float myPersonalityFearfulWeight < min=0.0 default=0.0 >
	float myPersonalityHungryWeight < min=0.0 default=0.0 >
}

class NPC < uid=auto >
{
	varmap myUIName	< bhv = "localization" >
	varmap myUIRankName	< bhv = "localization" >
	
	file myGraphObjectFile "" < bhv=asset assettype=object >
	
	file myVisualGenerationFile ""
	VisualGenerationWeights myBaseWeights
	file myIcon "rogue/ui/ClassIcons/UI-ClassIconDefault-01.tga"
	color myClassColor 0xffa0ffa0

	bool myUseOnlyBaseGraphObject FALSE
	bool myUseSessionConstantGraphObject TRUE
	
	AnimSystemType myAnimSystemType Human
	AnimSpecialization myAnimSpecialization None
	NPCGender myGender Male
	
	NPCArchetype myArchetype Unset
	AudioSwitch myArchetypeSwitch
	Faction* myDefaultFaction
	
	bool myIsHuman TRUE
	bool myUseOnlyVisualGeneration FALSE
	bool myHasLockedFacing FALSE
	bool myNeedsToFaceTargetToShoot TRUE
	bool myIsPlayerTargetable FALSE
	bool myIsTargetableBySkills TRUE
	bool myIgnoreThreats FALSE
	bool myClaimsPositionsDuringMovement TRUE
	bool myCanBlockPlayerMovement TRUE
	bool myWantsCorpseInteractionPoint FALSE
	bool myCanStepOutInLowCover TRUE
	bool myDisableUI FALSE
	
	LootTable* myLootTable
	file myEliteRankTable ""
	
	PrimaryWeapons myPrimaryWeapons
	PrimaryWeapons myVeteranPrimaryWeapons
	PrimaryWeapons myElitePrimaryWeapons
	PrimaryWeapons myNamedPrimaryWeapons
	SecondaryWeapons mySecondaryWeapons
	SecondaryWeapons myVeteranSecondaryWeapons
	SecondaryWeapons myEliteSecondaryWeapons
	SecondaryWeapons myNamedSecondaryWeapons
	
	ArmorPieces myArmorPieces
	string myAIVisionConeTypeName "GruntVisionCone"
	string myStaggerGaugeTypeName "DefaultStaggerGauge"
	DialogueCharacterList myDialogueCharacterNames
	bool myMatchDialogueCharacterWithVisual FALSE
	
	file mySensorProfile ""
	file myBehavior < bhv=asset assettype=behavior >
	file myNPCGearProgressionFile "rogue/game system data/juice/npcgear/test.mnpcgear" < bhv=asset assettype="npc gear progression" >
	file myDeathActionFile < bhv=asset assettype=behavior >
	file myHitbox "rogue/game system data/juice/agentcollision/playerHitboxStates.juice"
	file mySuppressionProfile "rogue/game system data/juice/ai/suppressionprofile/default_suppression_profile.juice"
	
	float myAddedBaseThreat 0.0
		
	AttributeProgression* myAttributeProgression
	NPCGear myGearProgression
	NPCGear myGearProgressionElite
	
	QuestGiver myQuestGiver
	
	WeakPointList myWeakPoints
	
	DetectionProfile* myDetectionProfile
	NonCombatantDetectionProfile* myNonCombatantDetectionProfile

	AimProfile* myAimProfile
	AimProfile* myReducedAimAimProfile
	AimProfile* myVeteranAimProfile
	AimProfile* myEliteAimProfile
	AimProfile* myNamedAimProfile
	
	ShootSequenceSelectorProfile* myShootSequenceSelectorProfile
	
	MotionPattern* myDefaultMotionPattern
	PatternProfile* myPatternProfile

	ActionThreatProfile* myActionThreatProfile
	BaseThreatProfile* myBaseThreatProfile
	
	NPCCombatConfig myNPCCombatConfig
	
	AILifeConfig* myAILifeConfig
	float myBiomass 1.0  < tooltip="Used by non-combatant to evaluate bullying relationships" >
	
	NPCSceneData myNPCSceneData
	
	bool myIgnorePathAvoidance FALSE

	file myBishopCharacterFile "rogue/_platforms/mobile/common/characters/b_cn_lmb_snp.tag"
	file myBishopStaticNodeFile ""
	
	NavigationCapability* myNavigationCapability
	
	SnapToGroundType mySnapToGroundType Ground 
	
	file myLeftFootstepGraphObject "rogue/graph objects/prop/ShoePrint-SnowLeft.mgraphobject" < bhv=asset assettype=object >
	file myRightFootstepGraphObject "rogue/graph objects/prop/ShoePrint-SnowRight.mgraphobject" < bhv=asset assettype=object >
	bool myUseDefaultFootsteps false < tooltip="This makes the game push default footsteps when this type of agent moves their foot bones enough." >
	float myDefaultFootstepsFloorHeight 0.1 < tooltip="The height that this agent type's footbone needs to go above to consider that foot up. In local space." >
	
	NPCSpecialRole myMinRole None

	bool myIsMultiAgent FALSE

	int myMinVisuals 1
	int myMaxVisuals 1
	
	bool myCanBeMeleePushed true
	float myMeleePushedCooldown 3.0

	float myAgentRadius 0.32

	bool myIgnoreUnderFireAnimation FALSE
	bool mySequenceHeadLookEnabled FALSE

	PhysicsCharacterProfile* myPhysicsPhantomProfile
}
